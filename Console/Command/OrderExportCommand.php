<?php
namespace Vaimo\ExportOrders\Console\Command;

use Magento\Framework\Filesystem\Io\File;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Framework\Convert\ConvertArray;
use Magento\Sales\Api\Data\OrderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SomeCommand
 */
class OrderExportCommand extends Command
{
    const BASE_DIR = "/var/www/html/";
    const ORDER_ID = 'order-id';

    /**
     * @var ConvertArray
     */
    private $convertArray;

    /**
     * @var File
     */
    private $file;

    /**
     * @var OrderSearchResultInterface
     */
    private $orderSearchResult;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    public function __construct(
        OrderSearchResultInterface $orderSearchResult,
        OrderRepositoryInterface $orderRepository,
        ConvertArray $convertArray,
        File $file
    )
    {
        $this->orderSearchResult = $orderSearchResult;
        $this->orderRepository = $orderRepository;
        $this->convertArray = $convertArray;
        $this->file = $file;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('orders:export');
        $this->setDescription('Export orders in a var directory.');
        $this->addOption(
            self::ORDER_ID,
            'i',
            InputOption::VALUE_OPTIONAL,
            'Export by order id'
        );

        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $orderId = $input->getOption(self::ORDER_ID);

        if ($orderId != 0) {
            $order = $this->orderRepository->get($orderId);
            $this->exportToXml($order);
            $output->writeln('<info>Exported to var folder.</info>');
        } else {
            $orders = $this->orderSearchResult->getItems();
            $this->exportToXml($orders);
            $output->writeln('<info>Exported to var folder.</info>');
        }
    }

    /**
     * @param $order
     * @param string $path
     * @throws \Exception
     */
    private function exportToXml($order, string $path='var')
    {
        $preparedOrder = $this->prepareOrderToExport($order);

        if (!\is_object($preparedOrder)) {
            foreach ($preparedOrder as $xml) {
                $this->saveFile($xml, $xml->xpath('/Order/OrderId')[0], $path);
            }
        } else {
            $this->saveFile($preparedOrder, $preparedOrder->xpath('/Order/OrderId')[0], $path);
        }
    }

    /**
     * @param \SimpleXMLElement $content
     * @param string|int $orderId
     * @param string $path
     * @throws \Exception
     */
    private function saveFile(\SimpleXMLElement $content, $orderId, string $path)
    {
        $xmlDocument = new \DOMDocument('1.0');
        $xmlDocument->preserveWhiteSpace = false;
        $xmlDocument->formatOutput = true;
        $xmlDocument->loadXML($content->asXml());

        $this->file->cd(self::BASE_DIR . $path);
        $this->file->write("order_$orderId.xml", $xmlDocument->saveXML());
    }

    /**
     * @param OrderInterface|OrderInterface[] $order
     * @return array|\SimpleXMLElement
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function prepareOrderToExport($order)
    {
        $preparedArray = [];
        if (\is_array($order)) {
            /** @var \Magento\Sales\Api\Data\OrderInterface $order */
            foreach ($order as $item) {
                $orderArray = $this->getOrderAsArray($item);
                \array_push($preparedArray, $orderArray);
            }
        } else {
            return $this->getOrderAsArray($order);
        }

        return $preparedArray;
    }

    /**
     * @param OrderInterface|OrderInterface[] $order
     * @return \SimpleXMLElement
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getOrderAsArray($order): \SimpleXMLElement
    {
        $orderArray = [
            "OrderId"        => $order->getIncrementId(),
            "CustomerName"   => $order->getCustomerFirstname() . " " . $order->getCustomerLasname(),
            "CustomerEmail" => $order->getCustomerEmail(),
            "IsGuest"        => $order->getCustomerIsGuest()
        ];

        $xmlContent = $this->convertArray->assocToXml($orderArray, "Order");
        $this->addItemsToOrder($xmlContent, $order->getItems());

        return $xmlContent;
    }

    /**
     * @param \SimpleXMLElement $xmlContent
     * @param OrderItemInterface[] $orderItems
     */
    private function addItemsToOrder(\SimpleXMLElement $xmlContent, $orderItems)
    {
        $xmlOrderItems = $xmlContent->addChild("OrderItems");

        /** @var \Magento\Sales\Api\Data\OrderItemInterface $item */
        foreach ($orderItems as $item) {
            $preparedItem = [
                "ItemId"    => $item->getProductId(),
                "ItemName"  => $item->getName(),
                "Sku"       => $item->getSku(),
                "Qty"       => $item->getQtyOrdered()
            ];

            $xmlOrderItem = $xmlOrderItems->addChild("OrderItem");

            foreach ($preparedItem as $key => $value) {
                $xmlOrderItem->addChild($key, (string) $value);
            }
        }
    }
}
